# What is SASS?
----------------------
- It is an extension to CSS than can help us write more flexible styles
- Use variables, functions, conditional statements and more..
- Split Our SASS files up into modules, making it easier to keep on top of CSS for large projects
- When we write in SASS, browsers will not understand our code
- It needs to be translated into CSS
- SASS in witten in Ruby

# What you Should know
----------------------
- Programming fundamentals - variables, functions, if stattements etc
- CSS

# Installing & Compiling Sass
----------------------
- http://www.sass-lang.com/install
- https://chocolatey.org/
- https://rubyinstaller.org/
- http://scout-app.io/ (open-source)
- https://prepros.io/ (paid)
- http://sass-lang.com/documentation/Sass/Script/Functions.html

- https://www.youtube.com/watch?v=ZF9h-Qd0O4Y (bootstrap 4 sass)

- http://refresh-sf.com/  (Online JavaScript/CSS/HTML Compressor)
- http://minifycode.com/html-minifier/ (HTML minifier)

# command code
- gem install sass
- sass -v
- sass style.scss output.css
- sass --watch style.scss:output.css
- sass --watch sass:css (sass= foldernameSASS, css = foldernameCSS)
- sass --watch sass:css --style expanded
- sass --watch sass:css --style compressed



